﻿using System;
using UnityEngine;

namespace Utilities
{
    public class Matrix2D
    {
        private Vector2 p;

        private Vector2 u;

        private Vector2 v;

        public Matrix2D(float px, float py, float ux, float uy, float vx, float vy)
        {
            p = new Vector2(px, py);
            u = new Vector2(ux, uy);
            v = new Vector2(vx, vy);
        }

        public Vector2 Multiply(float x, float y)
        {
            return p + u * x + v * y;
        }
    }
}
