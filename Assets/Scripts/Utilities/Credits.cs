﻿using UnityEngine;
using System.Collections;

public class Credits : MonoBehaviour
{
  public float ExitDelay = 2.0f;

	// Use this for initialization
	void Start ()
  {
    StartCoroutine(DelayExit(ExitDelay));
	}

  IEnumerator DelayExit(float time)
  {
    yield return new WaitForSeconds(time);

    ExitHandler.Instance.InstantExit();
  }
}
