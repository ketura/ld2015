﻿using UnityEngine;
using Vectrosity;
using System.Collections.Generic;

namespace Utilities
{
    public static class DrawUtils
    {
        public static VectorLine MakeX(Transform parent, float size, Color color)
        {
            var points = new List<Vector3>();
            points.Add(new Vector3(-size, -size));
            points.Add(new Vector3(size, size));
            points.Add(new Vector3(size, -size));
            points.Add(new Vector3(-size, size));

            var x = new VectorLine("cross", points, null, 2.5f, LineType.Discrete);
            x.drawTransform = parent;
            x.color = color;

            return x;
        }

        public static VectorLine MakeSquare(Transform parent, float size, Color color)
        {
            var square = new VectorLine("tank", new List<Vector3>(new Vector3[5]), null, 2.5f, LineType.Continuous);
            square.drawTransform = parent;
            square.MakeRect(new Vector3(-size, -size), new Vector3(size, size));
            square.color = color;

            return square;
        }

        public static VectorLine MakeRect(Transform parent, Vector3 topLeft, Vector3 bottomRight, Color color)
        {
            var square = new VectorLine("rect", new List<Vector3>(new Vector3[5]), null, 2.5f, LineType.Continuous);
            square.drawTransform = parent;
            square.MakeRect(topLeft, bottomRight);
            square.color = color;
            return square;
        }

        public static VectorLine MakeRect(Vector3 topLeft, Vector3 bottomRight, Color color)
        {
            var square = new VectorLine("rect", new List<Vector3>(new Vector3[5]), null, 2.5f, LineType.Continuous);
            square.MakeRect(topLeft, bottomRight);
            square.color = color;
            return square;
        }

        public static VectorLine MakeCircle(Vector3 center, float radius, Color color)
        {
            var circle = new VectorLine("circle", new List<Vector3>(new Vector3[16]), null, 2.5f, LineType.Continuous);
            circle.MakeCircle(center, radius);
            circle.color = color;
            return circle;
        }

        public static VectorLine MakeLine(Transform parent, List<Vector2> points, Color color, Material material = null)
        {
            var points3D = points.Map<Vector2, Vector3>(v => (Vector3)v);
            var line = new VectorLine("line", points3D, null, 2.5f, LineType.Continuous);
            line.drawTransform = parent;
            line.color = color;
            return line;
        }

        public static VectorLine MakeLine(string name, Transform parent, List<Vector2> points, Color color)
        {
            var points3D = points.Map<Vector2, Vector3>(v => (Vector3)v);
            var line = new VectorLine(name, points3D, null, 2.5f, LineType.Continuous);
            line.drawTransform = parent;
            line.color = color;
            return line;
        }

        public static VectorLine MakeLine(Transform parent, float length, Color color)
        {
            var points = new List<Vector3>();
            points.Add(new Vector3(0, 0));
            points.Add(new Vector3(0, length));

            var line = new VectorLine("line", points, null, 2.5f, LineType.Discrete);
            line.drawTransform = parent;
            line.color = color;
            return line;
        }

    }
}
