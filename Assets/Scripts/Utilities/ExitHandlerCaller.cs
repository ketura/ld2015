﻿using UnityEngine;
using System.Collections;

public class ExitHandlerCaller : MonoBehaviour {

    public void CallExitHandler()
    {
        ExitHandler.Instance.GracefulExit();
    }
}
