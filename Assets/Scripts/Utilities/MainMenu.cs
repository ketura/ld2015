﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour 
{
  public AudioClip MainMenuBGM;

  public string GameScene;

	// Use this for initialization
	void Start () 
  {
    if(MainMenuBGM != null)
      AudioManager.Instance.PlayClip(MainMenuBGM);
	}
	
	// Update is called once per frame
	void Update () 
  {
    if (Input.GetKeyDown("escape"))
      Exit();
	}

  public void Exit()
  {
    //Possibly change to 
    ExitHandler.Instance.GracefulExit();
  }

  public void Game()
  {
    if (GameScene == "")
    {
      Debug.LogError("Main menu has no Play scene set. Exiting instead.");
      Exit();
    }
    else
    {
      Debug.Log("Loading game scene " + GameScene);
      SceneManager.LoadScene(GameScene);
      //Application.LoadLevel(GameScene);
    }
  }

}
