﻿using UnityEngine;
using System.Collections;

namespace Game
{
    public class StartNewGame : MonoBehaviour
    {

        public void StartNewGameOnManager()
        {
            GameManager.Instance.StartNewGame();
        }
    }
}