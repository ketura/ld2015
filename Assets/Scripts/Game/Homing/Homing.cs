﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Game
{
    public interface IHomingReceiver
    {
        void ApplyHoming(Vector3 targetPosition, float rotationSpeed);
    }

    public class Homing : MonoBehaviour
    {
        public GameObject homingReceiverObject;
        public LayerMask layerMask;
        public float magnitude;
        public float radius;

        HomingTarget currentTarget;
        IHomingReceiver[] homingReceivers;

        void Start()
        {
            homingReceivers = homingReceiverObject.GetComponentsInChildren<IHomingReceiver>();
        }

        void Update()
        {
            if (currentTarget != null)
            {
                Vector2 difference = new Vector2(   currentTarget.transform.position.x - transform.position.x,
                                                    currentTarget.transform.position.y - transform.position.y);

                foreach (var receiver in homingReceivers)
                {
                    receiver.ApplyHoming(currentTarget.transform.position, magnitude);
                }
            }
            else
            {
                foreach (var collider in Physics2D.OverlapCircleAll(transform.position + transform.up * radius, radius, layerMask))
                {
                    var homingTarget = collider.GetComponent<HomingTarget>();
                    if (homingTarget)
                    {
                        currentTarget = homingTarget;
                        break;
                    }
                }
            }
        }

    }
}