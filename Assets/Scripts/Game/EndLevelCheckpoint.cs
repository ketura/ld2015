﻿using UnityEngine;
using System.Collections;

namespace Game
{
    public class EndLevelCheckpoint : Checkpoint
    {
        public override void OnReached()
        {
            GameManager.Instance.LoadNextLevel();
        }
    }
}
