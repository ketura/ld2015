﻿using UnityEngine;
using System;
using System.Collections;

public class FadeSpriteAlpha : MonoBehaviour {

    public SpriteRenderer sprite;
    public float duration;

    void Reset()
    {
        sprite = GetComponent<SpriteRenderer>();
    }

    IEnumerator Start()
    {
        float endTime = Time.time + duration;
        while (Time.time < endTime)
        {
            sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, Mathf.Lerp(0.0f, 1.0f, (endTime - Time.time) / duration));
            yield return null;
        }
        ObjectPoolController.Destroy(gameObject);
    }
}
