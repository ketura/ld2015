﻿using UnityEngine;
using System.Collections;

using Utilities;

using Vectrosity;

namespace Game
{
    public class DrawLocalLine : MonoBehaviour
    {
        public float length = 1;

        public Color color = Color.white;

        VectorLine line;

        void Awake()
        {
            line = DrawUtils.MakeLine(transform, length, color);
        }

        // Update is called once per frame
        void Update()
        {
            line.Draw();
        }

        void OnDestroy()
        {
            VectorLine.Destroy(ref line);
        }
    }
}