﻿using UnityEngine;
using System.Collections;

namespace Game
{
  public class TankDirection : MonoBehaviour
  {
    public bool Move;

    public float TurretAngle;

    public Animator ChassisAnim;
    public SpriteRenderer ChassisSprite;
    public Transform Chassis;

    public SpriteRenderer HorizTurret;
    public GameObject UpTurret;
    public GameObject DownTurret;

    public bool Horizontal = false;

    // Use this for initialization
    void Start()
    {
      if (Chassis != null)
      {
        lastPos = Chassis.transform.position;
        ChassisAnim.SetBool("Horizontal", Horizontal);
      }
      else
        lastPos = Vector2.zero;
    }

    private Vector2 lastPos;

    // Update is called once per frame
    void Update()
    {
      
      if (TurretAngle > 360)
        TurretAngle -= 360;
      if (TurretAngle < 0)
        TurretAngle += 360;

      //horizontal right
      if (TurretAngle <= 45 || TurretAngle >= 315)
      {
        HorizTurret.gameObject.SetActive(true);
        UpTurret.SetActive(false);
        DownTurret.SetActive(false);

        HorizTurret.transform.localRotation = Quaternion.Euler(0, 0, (float)TurretAngle);
        HorizTurret.flipY = false;
      }
      //horizontal left
      else if (TurretAngle >= 135 && TurretAngle <= 225)
      {
        HorizTurret.gameObject.SetActive(true);
        UpTurret.SetActive(false);
        DownTurret.SetActive(false);

        HorizTurret.transform.localRotation = Quaternion.Euler(0, 0, (float)TurretAngle);
        HorizTurret.flipY = true;
      }
      //turret up
      else if (TurretAngle < 135 && TurretAngle > 45)
      {
        HorizTurret.gameObject.SetActive(false);
        UpTurret.SetActive(true);
        DownTurret.SetActive(false);

        float angle = TurretAngle - 90;
        if (angle < 0)
          angle += 360;

        UpTurret.transform.localRotation = Quaternion.Euler(0, 0, angle);
      }
      //turret down
      else if (TurretAngle > 225 && TurretAngle < 315)
      {
        HorizTurret.gameObject.SetActive(false);
        UpTurret.SetActive(false);
        DownTurret.SetActive(true);

        float angle = TurretAngle + 90;
        if (angle > 360)
          angle -= 360;

        DownTurret.transform.localRotation = Quaternion.Euler(0, 0, angle);
      }

      if (Move)
      {

        Vector2 pos = (Vector2)Chassis.transform.position - lastPos;

        //if (pos.x != 0 && pos.y != 0)
          //Debug.Log(pos.x + ", " + pos.y);

        Horizontal = (Mathf.Abs(pos.x) > Mathf.Abs(pos.y));

        if (pos.x != pos.y)
          ChassisAnim.SetBool("Horizontal", Horizontal);

        lastPos = Chassis.transform.position;
      }

    }

    public Vector2 RotationVector()
    {
      return new Vector2(Mathf.Cos(TurretAngle), Mathf.Sin(TurretAngle));
    }
  }
}