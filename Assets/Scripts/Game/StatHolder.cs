﻿using UnityEngine;
using System.Collections;

namespace Game
{
  [System.Serializable]
  public class StatHolder : MonoBehaviour
  {
    public FloatStat speed;
    public Health health;
    public Repair repair;
    public Energy energy;
    public Recharge recharge;
    public Damage damage;
    public RoF rof;


  }
}