﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using System;
using System.Collections;
using System.Collections.Generic;
using Utilities;

namespace Game
{
    [System.Serializable]
    public class EnemyDistribution
    {
        [System.Serializable]
        public class EnemyWeightPair
        {
            public GameObject enemyPrefab;
            public float weight;
        }

        public List<EnemyWeightPair> enemyWeights = new List<EnemyWeightPair>();

        public List<GameObject> GeneratePrefabList(int enemyCount)
        {
            float totalWeight = 0.0f;
            foreach (var pair in enemyWeights)
                totalWeight += pair.weight;

            List<GameObject> returnList = new List<GameObject>();

            int currentIndex = 0;
            float currentWeight = enemyWeights[0].weight;
            float increment = (float)enemyCount / totalWeight;

            for (int i = 0; i < enemyCount; i++)
            {
                float val = ((float)i) * increment;
                if (val > currentWeight)
                {
                    if (currentIndex < enemyWeights.Count - 1)
                        currentIndex++;
                    currentWeight += enemyWeights[currentIndex].weight;
                }
                returnList.Add(enemyWeights[currentIndex].enemyPrefab);
            }

            returnList.Shuffle<GameObject>();

            return returnList;
        }
    }

    public class LevelParameters : ScriptableObject
    {
#if UNITY_EDITOR
        [MenuItem("Level/Create LevelParameters")]
        public static void CreateLevelParameters()
        {
            ScriptableObject asset = ScriptableObject.CreateInstance("LevelParameters");
            AssetDatabase.CreateAsset(asset, String.Format("Assets/{0}.asset", ("LevelParameters")));
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;
        }
#endif

        public float minRegionWidth = 5f;

        public float maxRegionWidth = 5f;

        public float minRegionHeight = 10f;

        public float maxRegionHeight = 10f;

        public int minCheckpoints = 5;

        public int maxCheckpoints = 5;

        public int minEnemies;
        public int maxEnemies;

        public EnemyDistribution enemyDistribution = new EnemyDistribution();

        public int maxModuleTier;
    }
}