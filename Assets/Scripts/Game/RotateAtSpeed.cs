﻿using UnityEngine;
using System.Collections;

public class RotateAtSpeed : MonoBehaviour {

    public float speed;
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(0.0f, 0.0f, speed * Time.deltaTime);
	}
}
