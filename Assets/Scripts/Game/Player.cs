﻿using UnityEngine;
using System.Collections;
using Vectrosity;
using System.Collections.Generic;
using Utilities;

namespace Game
{
    public class Player : MonoBehaviour
    {
        public FloatStat moveSpeed;
        public AudioClip rumbleSound;
        AudioSource instantiatedRumble;

        private float distanceTraveled = 0f;

        public TankDirection TankDir;

        public ModuleGroup primaryModules;
        public ModuleGroup secondaryModules;
        public ModuleGroup tertiaryModules;
        public ModuleGroup consumableModules;

        public void StartLevel()
        {
            distanceTraveled = 0.0f;
            if (rumbleSound) 
                instantiatedRumble = AudioManager.Instance.PlayClipLooping(rumbleSound, 0.35f);
        }

        public void Update()
        {
            UpdateInput();

            if (instantiatedRumble)
            {
                if (Time.timeScale == 0.0f)
                    instantiatedRumble.Pause();
                else if (!instantiatedRumble.isPlaying)
                    instantiatedRumble.Play();
            }

            if (Map.Instance.Loaded)
            {
                distanceTraveled += Time.deltaTime * moveSpeed.Amount;
                transform.position = Map.Instance.Path.GetPointAlong(distanceTraveled);
            }
            else
            {
                transform.position = Vector3.zero;
            }

            UpdateCamera();

            if (CheckpointManager.Instance.Loaded)
            {
                CheckpointManager.Instance.OnPlayerMoved(distanceTraveled + .5f);
            }
        }

        protected void UpdateInput()
        {
            if (InputManager.Instance.PrimaryButton.Pressed())
            {
                primaryModules.OnButtonPressed();
            }
            if (InputManager.Instance.PrimaryButton.Released())
            {
                primaryModules.OnButtonReleased();
            }

            if (InputManager.Instance.SecondaryButton.Pressed())
            {
                secondaryModules.OnButtonPressed();
            }
            if (InputManager.Instance.SecondaryButton.Released())
            {
                secondaryModules.OnButtonReleased();
            }
        }

        protected void UpdateCamera()
        {
            Camera.main.transform.position = new Vector3(transform.position.x, transform.position.y, -10f);
        }

        public void AddModule(ModuleType type, Module modulePrefab, bool clear)
        {
            switch(type)
            {
                case ModuleType.Primary:
                  primaryModules.Equip(modulePrefab, clear);
                  break;
                case ModuleType.Secondary:
                  secondaryModules.Equip(modulePrefab, clear);
                  break;
                case ModuleType.Tertiary:
                  tertiaryModules.Equip(modulePrefab, false);
                  break;
                case ModuleType.Consumable:
                  consumableModules.Equip(modulePrefab, clear);
                  break;
            }
        }
    }
}
