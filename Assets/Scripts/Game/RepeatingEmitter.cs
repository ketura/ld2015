﻿using UnityEngine;
using System.Collections;

public class RepeatingEmitter : MonoBehaviour {

    public GameObject prefab;
    public float period;

    // Use this for initialization
    IEnumerator Start()
    {
        while (true)
        {
            ObjectPoolController.Instantiate(prefab, transform.position, transform.rotation);

            yield return new WaitForSeconds(period);
        }
    }
}
