﻿using UnityEngine;
using System.Collections;

namespace Game
{
    public class Attackable : MonoBehaviour
    {
        public Health health;
        public Repair repair;

        public AudioClip tookDamageSound;
        public AudioClip deathSound;

        public GameObject spawnOnDeathPrefab;

        void Start()
        {
            health.CurrentHealth = health.Amount;
            StartCoroutine(Replenish());
        }

        public void TakeDamage(float amount)
        {
            health.CurrentHealth = Mathf.Clamp(health.CurrentHealth - amount, 0.0f, health.Amount);

            if(tookDamageSound)
                AudioManager.Instance.PlayClip(tookDamageSound);

            if (health.CurrentHealth == 0.0f)
            {
                if(deathSound)
                    AudioManager.Instance.PlayClip(deathSound);

                if (spawnOnDeathPrefab)
                    ObjectPoolController.Instantiate(spawnOnDeathPrefab, transform.position, transform.rotation);

                //TODO: actual death handling
                Destroy(gameObject);
            }
        }

    bool regen = false;
    IEnumerator Replenish()
    {
      if (!regen)
      {
        regen = true;

        while (regen)
        {
          yield return new WaitForSeconds(1.0f);
          health.CurrentHealth += repair.Amount;
        }

        regen = false;
      }
    }
  }
}