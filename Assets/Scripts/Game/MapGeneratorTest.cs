﻿using UnityEngine;
using Utilities;
using Vectrosity;

namespace Game
{
    public class MapGeneratorTest : MonoBehaviour
    {
        private VectorLine path;

        private void Start()
        {
            var gen = new MapGenerator();
            path = DrawUtils.MakeLine(transform, gen.Generate(), Color.grey);
        }

        private void Update()
        {
            path.Draw();
        }
    }
}
