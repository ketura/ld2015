﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

using Utilities;

namespace Game
{
    public class GameManager : Singleton<GameManager>
    {
        public int currentLevelIndex
        {
            get;
            private set;
        }

        public LevelParameters[] levelParameters;

        public LevelParameters CurrentLevelParameters
        {
            get
            {
                return levelParameters[currentLevelIndex];
            }
        }

        public GameObject playerPrefab;
        Player instantiatedPlayer;

        // flag for starting the game immediately
        // for testing the game directly from the scene
        public bool startGameOnInitialize;

        protected override void Initialize()
        {
            if (startGameOnInitialize)
            {
                StartCoroutine(StartNewGameCoroutine());
            }
        }

        public void StartNewGame()
        {
            StartCoroutine(StartNewGameCoroutine());
        }

        public IEnumerator StartNewGameCoroutine()
        {
            //TODO: player spawning and initialization
            if (instantiatedPlayer)
            {
                Destroy(instantiatedPlayer);
            }
            instantiatedPlayer = (Instantiate(playerPrefab) as GameObject).GetComponent<Player>();

            yield return null;

            StartCoroutine(LoadLevel(0));
        }

        [ContextMenu("LoadNextLevel")]
        public void LoadNextLevel()
        {
            if (currentLevelIndex >= levelParameters.Length - 1)
            {
                Debug.Log("No more levels.");

                SceneManager.LoadScene("you win");
            }
            else
            {
                StartCoroutine(LoadLevel(++currentLevelIndex));
            }
        }

        public IEnumerator LoadLevel(int index)
        {
            //TODO: generate map, checkpoints, enemy
            //use levelParameters[index]
            currentLevelIndex = index;

            SceneManager.LoadScene("Game");
            yield return null;

            var levelInfo = levelParameters[index];
            Map.Instance.Load(levelInfo.minRegionWidth, levelInfo.maxRegionWidth, levelInfo.minRegionHeight, levelInfo.maxRegionHeight);
            Map.Instance.GenerateEnemySpawnPoints();
            CheckpointManager.Instance.Load(levelInfo.minCheckpoints, levelInfo.maxCheckpoints);

            List<GameObject> enemyPrefabList = levelInfo.enemyDistribution.GeneratePrefabList(UnityEngine.Random.Range(levelInfo.minEnemies, levelInfo.maxEnemies));
            Map.Instance.SpawnEnemies(enemyPrefabList);
            /*string logString = "";
            foreach (var go in enemyPrefabList)
            {
                logString += go.name;
                logString += ", ";
            }
            Debug.Log(logString);*/


            instantiatedPlayer.StartLevel();
        }
    }
}