﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Game
{
  public class BarUpdater : MonoBehaviour
  {
    public Text HealthDisplay;
    public Text RepairDisplay;
    public Slider HealthBar;

    public Text EnergyDisplay;
    public Text RechargeDisplay;
    public Slider EnergyBar;

    private StatAccessor stats;
    // Use this for initialization
    void Start()
    {
      Player p = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
      if (p == null)
        Debug.LogError("Could not find player!");
      else
        stats = p.GetComponent<StatAccessor>();
    }

    // Update is called once per frame
    void Update()
    {
      HealthDisplay.text = "" + ((int)((Health)stats.GetStat(Stats.Health)).CurrentHealth).ToString();
      RepairDisplay.text = "+" + stats.GetStat(Stats.Heal).ToString();
      HealthBar.maxValue = stats.GetStat(Stats.Health).Amount;
      HealthBar.value = ((Health)stats.GetStat(Stats.Health)).CurrentHealth;

      EnergyDisplay.text = "" + ((Energy)stats.GetStat(Stats.Energy)).CurrentEnergy;
      RechargeDisplay.text = "+" + stats.GetStat(Stats.Recharge).Amount;
      EnergyBar.maxValue = stats.GetStat(Stats.Energy).Amount;
      EnergyBar.value = ((Energy)stats.GetStat(Stats.Energy)).CurrentEnergy;

    }
  }
}