﻿using UnityEngine;
using System.Collections;
using Utilities;
using Vectrosity;

namespace Game
{
  public class CanvasManager : Singleton<CanvasManager>
  {
    public Canvas VectorCanvas;
    public Canvas PathCanvas;
    public Canvas HUDCanvas;
    public Canvas UICanvas;
    public Camera UICamera;

    protected override void Initialize()
    {
      //Debug.Log(VectorLine.SetupVectorCanvas());
      //VectorLine.SetCanvasCamera(UICamera);
    }

  }
}
