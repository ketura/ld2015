﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Game
{
  public enum CardState { None, Selected, Confirmed}

  public class Card : MonoBehaviour
  {
    public CardState state;

    public Vector3 ScaleFactor;

    public Text NameText;
    public Text DescText;
    public Image Icon;

    private Module mod;
    private Player p;

    void Start()
    {
      p = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }


    void Update()
    {
      switch(state)
      {
        case CardState.None:
          transform.localScale = Vector3.one;
          break;
        case CardState.Selected:
          transform.localScale = ScaleFactor;
          break;
        case CardState.Confirmed:
          transform.localScale = Vector3.one;
          ModulePicker.Instance.DeactivatePicker();
          if (mod)
          {
            p.AddModule(mod.Type, mod, true);
            mod = null;
          }

          state = CardState.None;
          break;
      }
    }

    public void Select()
    {
      if (Time.timeScale == 0)
      {
        if (state == CardState.Selected)
          state = CardState.Confirmed;
        else
          state = CardState.Selected;
      }
    }

    public void Deselect()
    {
      state = CardState.None;
    }

    public void ChangeCard(Module mod)
    {
      this.mod = mod;
      //TODO: Add the stat changes to the end of the desc text
      NameText.text = mod.ModuleName;
      DescText.text = mod.Description;
      Icon.sprite = mod.Icon;
    }
  }
}