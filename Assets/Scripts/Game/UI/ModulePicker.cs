﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Utilities;


namespace Game
{
  public class ModulePicker : Singleton<ModulePicker>
  {
    public List<Module> ModulePool;

    public bool PickActive { get; private set; }
    public AudioClip music;
    public Card LeftCard;
    public Card RightCard;

    public Canvas PickerCanvas;

    public List<Module> Primaries;
    public List<Module> Secondaries;
    public List<Module> Tertiaries;
    public List<Module> Consumables;

    public void ActivatePicker()
    {
      Time.timeScale = 0;
      AudioManager.Instance.PlayMusic(music);

      LeftCard.state = CardState.None;
      RightCard.state = CardState.None;

      Module left = null;
      Module right = null;

      while(left == null || right == null)
      {
        Debug.Log("attempt");
        if(left == null)
          left = ModulePool[Random.Range(0, ModulePool.Count - 1)];

        if(right == null)
          right = ModulePool[Random.Range(0, ModulePool.Count - 1)];

        if (left.Tier > GameManager.Instance.CurrentLevelParameters.maxModuleTier)
          left = null;

        if (right.Tier > GameManager.Instance.CurrentLevelParameters.maxModuleTier)
          right = null;

        Debug.Log(left);
        Debug.Log(right);
      }

      LeftCard.ChangeCard(left);
      RightCard.ChangeCard(right);

      CanvasManager.Instance.VectorCanvas.enabled = false;
      PickerCanvas.enabled = true;
    }

    public void DeactivatePicker()
    {
      AudioManager.Instance.ResumeMainMusic();
      Time.timeScale = 1;
      CanvasManager.Instance.VectorCanvas.enabled = true;
      PickerCanvas.enabled = false;
    }

    void Update()
    {
      if (InputManager.Instance.PrimaryButton.Pressed())
      {
        LeftCard.Select();
        RightCard.Deselect();
      }

      if (InputManager.Instance.SecondaryButton.Pressed())
      {
        RightCard.Select();
        LeftCard.Deselect();
      }
    }
  }
}
