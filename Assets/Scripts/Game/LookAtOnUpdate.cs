﻿using UnityEngine;
using System.Collections;

namespace Game
{
    public class LookAtOnUpdate : MonoBehaviour
    {
        public Player player;


        // Update is called once per frame
        void Update()
        {
            Transform target = null;
            if (TargetForEnemies.Instance)
                target = TargetForEnemies.Instance.transform;

            if (target)
            {
                if (player)
                {
                    Vector3 diff = target.transform.position - transform.position;
                    diff.Normalize();

                    float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
                    Quaternion rot = Quaternion.Euler(0f, 0f, rot_z);
                    player.TankDir.TurretAngle = rot.eulerAngles.z;
                    //Debug.Log(rot.eulerAngles.z);
                }
                else
                {
                    Quaternion rotation = Quaternion.LookRotation
                   (target.transform.position - transform.position, transform.TransformDirection(Vector3.forward));
                    transform.rotation = new Quaternion(0, 0, rotation.z, rotation.w);

                }
            }
        }
    }
}