﻿using UnityEngine;
using System.Collections.Generic;
using Utilities;

namespace Game
{
    public class CheckpointManager : Singleton<CheckpointManager>
    {
        public GameObject prefab;
        public GameObject endLevelPrefab;

        public bool Loaded { get; private set; }

        private List<float> distances = new List<float>();

        private readonly List<Checkpoint> checkpoints = new List<Checkpoint>();

        public void Load(int min, int max)
        {
            var path = Map.Instance.Path;
            GenerateDistances(Random.Range(min, max));

            foreach (float distance in distances)
            {
                var obj = Instantiate(prefab, path.GetPointAlong(distance), Quaternion.identity) as GameObject;
                var checkpoint = obj.GetComponent<Checkpoint>();
                checkpoint.distance = distance;
                checkpoints.Add(checkpoint);
            }

            var endobj = Instantiate(endLevelPrefab, path.GetFinalEndpoint(), Quaternion.identity) as GameObject;
            var endcheckpoint = endobj.GetComponent<Checkpoint>();
            endcheckpoint.distance = path.GetLength();
            checkpoints.Add(endcheckpoint);

            Loaded = true;
        }

        private void GenerateDistances(int count)
        {
            float delta = Map.Instance.Path.GetLength() / (count + 1);

            for (int i = 1; i <= count; i++)
            {
                distances.Add(i * delta);
            }
        }

        public void OnPlayerMoved(float distanceAtFront)
        {
            foreach (var checkpoint in checkpoints)
            {
                if (!checkpoint.reached && checkpoint.distance < distanceAtFront)
                {
                    checkpoint.OnReached();
                }
            }
        }
    }
}
