﻿using UnityEngine;
using Utilities;
using System.Collections.Generic;

namespace Game
{
    public class MapGenerator
    {
        private class Region
        {
            public int Type { get; private set; }

            public float Left { get; private set; }

            public float Right { get; private set; }

            public float Bottom { get; private set; }

            public float Top { get; private set; }

            public float Width { get { return Right - Left; } }

            public float Height { get { return Top - Bottom; } }

            public float StartX { get; private set; }

            public float StartY { get; private set; }

            public Vector2 Start { get { return new Vector2(StartX, StartY); } }

            public float EndX { get; private set; }

            public float EndY { get; private set; }

            public Vector2 End { get { return new Vector2(EndX, EndY); } }

            public Region(int type, float left, float right, float bottom, float top)
            {
                Type = type;
                Left = left;
                Right = right;
                Bottom = bottom;
                Top = top;
            }

            public void SetStart(float x, float y)
            {
                StartX = x;
                StartY = y;
            }

            public void SetEnd(float x, float y)
            {
                EndX = x;
                EndY = y;
            }

            public void ConnectTo(Region region)
            {
                float x = 0;

                if (Mathf.Approximately(Left, region.Left) || Mathf.Approximately(Left, region.Right))
                {
                    x = Left;
                }
                else if (Mathf.Approximately(Right, region.Left) || Mathf.Approximately(Right, region.Right))
                {
                    x = Right;
                }

                float y = 0;

                if (Mathf.Approximately(Top, region.Top) || Mathf.Approximately(Top, region.Bottom))
                {
                    y = Top;
                }
                else if (Mathf.Approximately(Bottom, region.Top) || Mathf.Approximately(Bottom, region.Bottom))
                {
                    y = Bottom;
                }

                SetEnd(x, y);
                region.SetStart(x, y);
            }

            public void Finish()
            {
                switch (Type)
                {
                    case 1:
                    case 8:
                        SetEnd(Right, Random.Range(1, 2) == 1 ? Top : Bottom);
                        break;

                    case 2:
                    case 3:
                        SetEnd(Random.Range(1, 2) == 1 ? Left : Right, Top);
                        break;

                    case 4:
                    case 5:
                        SetEnd(Left, Random.Range(1, 2) == 1 ? Top : Bottom);
                        break;

                    default:
                        SetEnd(Random.Range(1, 2) == 1 ? Left : Right, Bottom);
                        break;
                }
            }

            public Matrix2D GetMatrix2D()
            {
                switch (Type)
                {
                    case 1:
                        return new Matrix2D(StartX, StartY, 0, Height, Width, 0);

                    case 2:
                        return new Matrix2D(StartX, StartY, Width, 0, 0, Height);

                    case 3:
                        return new Matrix2D(StartX, StartY, -Width, 0, 0, Height);

                    case 4:
                        return new Matrix2D(StartX, StartY, 0, Height, -Width, 0);

                    case 5:
                        return new Matrix2D(StartX, StartY, 0, -Height, -Width, 0);

                    case 6:
                        return new Matrix2D(StartX, StartY, -Width, 0, 0, -Height);

                    case 7:
                        return new Matrix2D(StartX, StartY, Width, 0, 0, -Height);

                    default:
                        return new Matrix2D(StartX, StartY, 0, -Height, Width, 0);
                }
            }

            public static Region CreateFromPoints(int type, float x1, float y1, float x2, float y2)
            {
                float left, right, bottom, top;

                if (x1 < x2)
                {
                    left = x1;
                    right = x2;
                }
                else
                {
                    left = x2;
                    right = x1;
                }

                if (y1 < y2)
                {
                    bottom = y1;
                    top = y2;
                }
                else
                {
                    bottom = y2;
                    top = y1;
                }

                return new Region(type, left, right, bottom, top);
            }

            public static Region CreateFromPointAndSize(int type, float x, float y, float w, float h)
            {
                switch (type)
                {
                    case 1:
                        return CreateFromPoints(type, 0 + x, 0 + y, h + x, w + y);

                    case 2:
                        return CreateFromPoints(type, 0 + x, 0 + y, w + x, h + y);

                    case 3:
                        return CreateFromPoints(type, 0 + x, 0 + y, -w + x, h + y);

                    case 4:
                        return CreateFromPoints(type, 0 + x, 0 + y, -h + x, w + y);

                    case 5:
                        return CreateFromPoints(type, 0 + x, 0 + y, -h + x, -w + y);

                    case 6:
                        return CreateFromPoints(type, 0 + x, 0 + y, -w + x, -h + y);

                    case 7:
                        return CreateFromPoints(type, 0 + x, 0 + y, w + x, -h + y);

                    default:
                    return CreateFromPoints(type, 0 + x, 0 + y, h + x, -w + y);            }
            }

            public static Region CreateFromRegionAndSize(int type, Region previous, float w, float h)
            {
                switch (type)
                {
                    case 1:
                    case 2:
                        return CreateFromPointAndSize(type, previous.Right, previous.Top, w, h);

                    case 3:
                    case 4:
                        return CreateFromPointAndSize(type, previous.Left, previous.Top, w, h);

                    case 5:
                    case 6:
                        return CreateFromPointAndSize(type, previous.Left, previous.Bottom, w, h);

                    default:
                        return CreateFromPointAndSize(type, previous.Right, previous.Bottom, w, h);
                }
            }
        }

        private readonly int[] r18 = { 1, 2, 7, 8 };

        private readonly int[] r23 = { 1, 2, 3, 4 };

        private readonly int[] r45 = { 3, 4, 5, 6 };

        private readonly int[] r67 = { 5, 6, 7, 8 };

        private readonly List<Region> regions = new List<Region>();

        private readonly List<Vector2> path = new List<Vector2>();

        public float MinRegionWidth { get; set; }

        public float MaxRegionWidth { get; set; }

        public float MinRegionHeight { get; set; }

        public float MaxRegionHeight { get; set; }

        public MapGenerator()
        {
            MinRegionWidth = 1f;
            MaxRegionWidth = 1.5f;
            MinRegionHeight = 2f;
            MaxRegionHeight = 3.5f;
        }

        public List<Vector2> Generate()
        {
            NextRegion(Random.Range(1, 8), null, 3);
            ConnectRegions();
            BuildPath();
            return path;
        }

        private void NextRegion(int type, Region previous, int n)
        {
            if (n == 0)
            {
                return;
            }

            Region region;
            float w = Random.Range(MinRegionWidth, MaxRegionWidth);
            float h = Random.Range(MinRegionHeight, MaxRegionHeight);

            if (previous == null)
            {
                region = Region.CreateFromPointAndSize(type, 0, 0, w, h);
            }
            else
            {
                region = Region.CreateFromRegionAndSize(type, previous, w, h);
            }

            regions.Add(region);
            NextRegion(FindNextRegion(type), region, n - 1);
        }

        private int FindNextRegion(int type)
        {
            switch (type)
            {
                case 1:
                case 8:
                    return r18[Random.Range(1, 4)];

                case 2:
                case 3:
                    return r23[Random.Range(1, 4)];

                case 4:
                case 5:
                    return r45[Random.Range(1, 4)];

                default:
                    return r67[Random.Range(1, 4)];
            }
        }

        private void ConnectRegions()
        {
            regions[0].SetStart(0, 0);
            regions[0].ConnectTo(regions[1]);
            regions[1].ConnectTo(regions[2]);
            regions[2].Finish();
        }

        private void BuildPath()
        {
            path.Add(regions[0].Start);

            foreach (var region in regions)
            {
                BuildPath(region);
            }
        }

        private void BuildPath(Region region)
        {
            var m = region.GetMatrix2D();
            float x;

            path.Add(m.Multiply(0f, .25f));

            x = Random.value;
            path.Add(m.Multiply(x, .25f));
            path.Add(m.Multiply(x, .5f));

            x = Random.value;
            path.Add(m.Multiply(x, .5f));
            path.Add(m.Multiply(x, .75f));

            x = Mathf.Approximately(region.StartX, region.EndX) || Mathf.Approximately(region.StartY, region.EndY) ? 0f : 1f;
            path.Add(m.Multiply(x, .75f));
            path.Add(m.Multiply(x, 1f));
        }
    }
}