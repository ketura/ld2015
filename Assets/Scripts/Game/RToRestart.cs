﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class RToRestart : MonoBehaviour {

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Debug.Log("R");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
	}
}
