﻿using UnityEngine;
using System.Collections.Generic;
using Vectrosity;
using Utilities;

namespace Game
{
    public class Checkpoint : MonoBehaviour
    {
        public float distance;

        public bool reached = false;

        public virtual void OnReached()
        {
            reached = true;
            Debug.Log("Reached checkpoint: " + distance);
            ModulePicker.Instance.ActivatePicker();
        }
    }
}
