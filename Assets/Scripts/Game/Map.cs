﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vectrosity;
using Utilities;

namespace Game
{
    public class Map : Singleton<Map>
    {
        private List<Vector2> path;

        private VectorLine pathLine;

        public List<Vector2> Path { get { return path; } }

        public Material pathMaterial;

        public bool Loaded { get; private set; }

        public void Load(float minRegionWidth, float maxRegionWidth, float minRegionHeight, float maxRegionHeight)
        {
            var gen = new MapGenerator();
            gen.MinRegionWidth = minRegionWidth;
            gen.MaxRegionWidth = maxRegionWidth;
            gen.MinRegionHeight = minRegionHeight;
            gen.MaxRegionHeight = maxRegionHeight;
            path = gen.Generate();
            CanvasManager i = CanvasManager.Instance;
            pathLine = DrawUtils.MakeLine("Path", transform, path, Color.grey);
            pathLine.material = pathMaterial;

            //pathLine.SetCanvas(CanvasManager.Instance.PathCanvas);
      //Debug.Log(CanvasManager.Instance.PathCanvas);

            Loaded = true;

        }

        #region Enemy spawners

        private List<Vector2> enemySpawnPoints = new List<Vector2>();

        private float spawnThreshold = 1f;

        private float spawnOffset = .5f;

        private List<VectorLine> spawnLines = new List<VectorLine>();

        public void GenerateEnemySpawnPoints()
        {
            var path = Map.Instance.Path;
            var a = path[0];

            for (int i = 1; i < path.Count; i++)
            {
                var b = path[i];
                GenerateEnemySpawnPoints(a, b);
                a = b;
            }
        }

        public void SpawnEnemies(List<GameObject> enemyPrefabs)
        {
            for (int e = 0; e < enemyPrefabs.Count; e++)
            {
                int i = (int)Mathf.Round(Mathf.Lerp(0, enemySpawnPoints.Count, (float)e / enemyPrefabs.Count));
                var prefab = enemyPrefabs[e];
                var sp = enemySpawnPoints[i];
                Instantiate(prefab, (Vector3)sp, Quaternion.identity);
            }
        }

        private void GenerateEnemySpawnPoints(Vector2 a, Vector2 b)
        {
            float totalLength = Vector2.Distance(a, b);
            float length = totalLength - spawnThreshold * 2;

            if (length <= 0)
            {
                return;
            }

            var leftDistance = spawnThreshold;
            var rightDistance = spawnThreshold + length;

            var leftOrigin = MoveAlong(a, b, leftDistance);
            var rightOrigin = MoveAlong(a, b, rightDistance);

            var norm = (b - a).normalized;
            var p = leftOrigin + (new Vector2(-norm.y, norm.x) * spawnOffset);
            var q = rightOrigin + (new Vector2(norm.y, -norm.x) * spawnOffset);

            enemySpawnPoints.Add(p);
            enemySpawnPoints.Add(q);

            spawnLines.Add(DrawUtils.MakeCircle(p, .25f, Color.green));
            spawnLines.Add(DrawUtils.MakeCircle(q, .25f, Color.green));
        }

        private static Vector2 MoveAlong(Vector2 a, Vector2 b, float dist)
        {
            return Vector2.Lerp(a, b, dist / Vector2.Distance(a, b));
        }

        #endregion

        private void Update()
        {
            if (Loaded)
            {
                pathLine.Draw();

//                foreach (var spawnPoint in spawnLines)
//                {
//                    spawnPoint.Draw();
//                }
            }
        }
    }
}
