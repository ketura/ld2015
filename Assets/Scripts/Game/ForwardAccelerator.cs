﻿using UnityEngine;
using System.Collections;

namespace Game
{
    public class ForwardAccelerator : MonoBehaviour
    {
        public Rigidbody2D myRigidbody;
        public float acceleration;
        public float maxSpeed;

        // Update is called once per frame
        void Update()
        {
            myRigidbody.velocity += new Vector2(-transform.up.x, -transform.up.y) * acceleration * Time.deltaTime;
            if (myRigidbody.velocity.magnitude > maxSpeed)
                myRigidbody.velocity = myRigidbody.velocity.normalized * maxSpeed;
        }
    }
}