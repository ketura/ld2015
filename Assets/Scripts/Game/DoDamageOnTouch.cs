﻿using UnityEngine;
using System.Collections;

namespace Game
{
    public class DoDamageOnTouch : MonoBehaviour
    {
        public Attackable myAttackable;
        public float damageAmount;

        void OnCollisionEnter2D(Collision2D coll)
        {
            var attackable = coll.gameObject.GetComponent<Attackable>();
            if (attackable)
            {
                attackable.TakeDamage(damageAmount);
                myAttackable.TakeDamage(99999.0f);
            }

        }
    }
}