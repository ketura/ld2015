﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;


public class LoadSceneAfterTime : MonoBehaviour {

    public string sceneName;
    public float duration;

	// Use this for initialization
	IEnumerator Start () {
        yield return new WaitForSeconds(duration);

        SceneManager.LoadScene(sceneName);
	}
	
}
