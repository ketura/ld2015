﻿using UnityEngine;
using System.Collections;

namespace Game
{
    [System.Serializable]
    public class Health : FloatStat
    {

        public float CurrentHealth
        {
            get;
            set;
        }

        public override float MultiplicativeModifier
        {
            get
            {
                return base.MultiplicativeModifier;
            }

            set
            {
                CurrentHealth *= value;
                base.MultiplicativeModifier = value;
            }
        }

        public override float AdditiveModifier
        {
            get
            {
                return base.AdditiveModifier;
            }

            set
            {
                CurrentHealth += value;
                base.AdditiveModifier = value;
            }
        }
    }
}