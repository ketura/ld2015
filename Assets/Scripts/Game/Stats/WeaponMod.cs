﻿using UnityEngine;
using System.Collections;

namespace Game
{

  public class WeaponMod : MonoBehaviour
  {
    public Damage damage;
    public RoF rof;

    public float DPS()
    {
      return damage.Amount / rof.Amount;
    }


  }
}
