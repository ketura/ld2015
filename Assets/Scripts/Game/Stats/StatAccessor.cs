﻿using UnityEngine;
using System.Collections;

namespace Game
{
    public enum Stats
    {
        Health,
        MoveSpeed,
        Energy,
        Recharge,
        Heal,
        Damage,
        RoF
    }

    public class StatAccessor : MonoBehaviour
    {
        public Attackable attackable;
        public Player player;

    private WeaponMod mod;
    private EnergyReserve reserve;

    void Start()
    {
      mod = player.GetComponent<WeaponMod>();
      reserve = player.GetComponent<EnergyReserve>();
    }

        void Reset()
        {
            attackable = GetComponent<Attackable>();
            player = GetComponent<Player>();
        }

        public FloatStat GetStat(Stats stat)
        {
            switch (stat)
            {
                case Stats.Health:
                    return attackable.health;

                case Stats.MoveSpeed:
                    return player.moveSpeed;

        case Stats.Energy:
          return reserve.energy;

        case Stats.Recharge:
          return reserve.recharge;

        case Stats.Heal:
          return attackable.repair;

        case Stats.Damage:
          return mod.damage;

        case Stats.RoF:
          return mod.rof;

                default:
                    Debug.LogError("Couldn't get stat: " + stat.ToString());
                    return null;
            }
        }
    }
}