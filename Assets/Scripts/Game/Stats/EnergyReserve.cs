﻿using UnityEngine;
using System.Collections;

namespace Game
{

  public class EnergyReserve : MonoBehaviour
  {
    public Energy energy;
    public Recharge recharge;

    void Start()
    {
      energy.CurrentEnergy = energy.Amount;
      StartCoroutine(Replenish());
    }

    bool regen = false;
    IEnumerator Replenish()
    {
      if(!regen)
      {
        regen = true;

        while (regen)
        {
          yield return new WaitForSeconds(1.0f);
          energy.CurrentEnergy += recharge.Amount;
          if (energy.CurrentEnergy > energy.Amount)
            energy.CurrentEnergy = energy.Amount;
        }

        regen = false;
      }
    }
   
  }
}
