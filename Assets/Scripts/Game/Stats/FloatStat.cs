﻿using UnityEngine;
using System.Collections;

namespace Game
{
    [System.Serializable]
    public class FloatStat
    {
        public string StatName = "";

        [SerializeField]
        protected float _baseValue;
        public float Base
        {
            get
            {
                return _baseValue;
            }
        }

        public bool DisplayDecimal = false;

        protected float _additiveModifier = 0.0f;
        public virtual float AdditiveModifier
        {
            get
            {
                return _additiveModifier;
            }

            set
            {
                _additiveModifier = value;  
            }
        }

        protected float _multiplicativeModifier = 1.0f;
        public virtual float MultiplicativeModifier
        {
            get
            {
                return _multiplicativeModifier;
            }

            set
            {
                _multiplicativeModifier = value;
            }
        }

        public float Amount
        {
            get
            {
                return (Base * MultiplicativeModifier) + AdditiveModifier;
            }
        }

        public override string ToString()
        {
            if (DisplayDecimal)
                return Amount.ToString();
            return ((int)Amount).ToString();
        }
    }
}
