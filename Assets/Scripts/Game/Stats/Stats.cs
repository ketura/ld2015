﻿using UnityEngine;
using System.Collections;

namespace Game
{
  [System.Serializable]
  public class Energy : FloatStat
  {
    public float CurrentEnergy { get; set; }
    public override float MultiplicativeModifier
    {
      get
      {
        return base.MultiplicativeModifier;
      }

      set
      {
        CurrentEnergy *= value;
        base.MultiplicativeModifier = value;
      }
    }

    public override float AdditiveModifier
    {
      get
      {
        return base.AdditiveModifier;
      }

      set
      {
        CurrentEnergy += value;
        base.AdditiveModifier = value;
      }
    }
  }

  [System.Serializable]
  public class Recharge : FloatStat
  {

  }

  [System.Serializable]
  public class Repair : FloatStat
  {

  }

  [System.Serializable]
  public class Damage : FloatStat
  {

  }

  [System.Serializable]
  public class RoF : FloatStat
  {

  }

  [System.Serializable]
  public class Speed : FloatStat
  {

  }
}