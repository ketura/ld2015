﻿using UnityEngine;
using System.Collections;

public class ExplosionScaler : MonoBehaviour {

    public float initialScale;
    public float endScale;
    public float duration;

	// Use this for initialization
	IEnumerator Start () {

        float endTime = Time.time + duration;
        while (Time.time < endTime)
        {
            float scale = Mathf.Lerp(endScale, initialScale, (endTime - Time.time) / duration);
            transform.localScale = new Vector3(scale, scale, scale);
            yield return null;
        }
    }
}
