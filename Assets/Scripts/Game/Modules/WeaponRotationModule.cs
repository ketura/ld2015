﻿using UnityEngine;
using System.Collections;
using System;

namespace Game
{
    public class WeaponRotationModule : Module
    {
        public float maxSpeed = 200f;
        public float acceleration = 1000f;
        public bool rotateWhileButtonDown;

        float currentRotationSpeed = 0.0f;
        bool buttonDown;

        private TankDirection Turret;

        void Start()
        {
            Turret = owningPlayer.TankDir;
        }

        void Update()
        {
            if ((buttonDown && rotateWhileButtonDown) || (!buttonDown && !rotateWhileButtonDown))
            {
                currentRotationSpeed = Mathf.Clamp(currentRotationSpeed + acceleration * Time.deltaTime, 0.0f, maxSpeed);
            }
            else
            {
                currentRotationSpeed = 0.0f;
            }

            //transform.Rotate(0.0f, 0.0f, currentRotationSpeed * Time.deltaTime);
            Rotate(currentRotationSpeed * Time.deltaTime);
        }

        public override void OnButtonDown()
        {
            buttonDown = true;
        }

        public override void OnButtonUp()
        {
            buttonDown = false;
        }

        private void Rotate(float angle)
        {
            Turret.TurretAngle += angle;
        }

        public override void OnEquip()
        {
            //throw new NotImplementedException();
        }

        public override void OnRemove()
        {
            //throw new NotImplementedException();
        }
  }
}