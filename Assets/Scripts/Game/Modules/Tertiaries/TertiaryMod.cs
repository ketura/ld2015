﻿using UnityEngine;
using System.Collections;
using System;

namespace Game
{

  public class TertiaryMod : Module
  {

    public StatHolder AdditiveStats;
    public StatHolder MultiplicitiveStats;


    void Start()
    {
      Debug.Log("t: " + owningPlayer);
    }

    public override void OnEquip()
    {
      Attackable a = base.owningPlayer.GetComponent<Attackable>();
      EnergyReserve er = owningPlayer.GetComponent<EnergyReserve>();
      WeaponMod wm = owningPlayer.GetComponent<WeaponMod>();

      owningPlayer.moveSpeed.AdditiveModifier += AdditiveStats.speed.Amount;
      a.health.AdditiveModifier += AdditiveStats.health.Amount;
      a.repair.AdditiveModifier += AdditiveStats.repair.Amount;
      er.energy.AdditiveModifier += AdditiveStats.energy.Amount;
      er.recharge.AdditiveModifier += AdditiveStats.recharge.Amount;
      wm.damage.AdditiveModifier += AdditiveStats.damage.Amount;
      wm.rof.AdditiveModifier += AdditiveStats.rof.Amount;

      owningPlayer.moveSpeed.MultiplicativeModifier += MultiplicitiveStats.speed.Amount;
      a.health.MultiplicativeModifier += MultiplicitiveStats.health.Amount;
      a.repair.MultiplicativeModifier += MultiplicitiveStats.repair.Amount;
      er.energy.MultiplicativeModifier += MultiplicitiveStats.energy.Amount;
      er.recharge.MultiplicativeModifier += MultiplicitiveStats.recharge.Amount;
      wm.damage.MultiplicativeModifier += MultiplicitiveStats.damage.Amount;
      wm.rof.MultiplicativeModifier += MultiplicitiveStats.rof.Amount;
    }

    public override void OnRemove()
    {
      Attackable a = owningPlayer.GetComponent<Attackable>();
      EnergyReserve er = owningPlayer.GetComponent<EnergyReserve>();
      WeaponMod wm = owningPlayer.GetComponent<WeaponMod>();

      owningPlayer.moveSpeed.AdditiveModifier -= AdditiveStats.speed.Amount;
      a.health.AdditiveModifier -= AdditiveStats.health.Amount;
      a.repair.AdditiveModifier -= AdditiveStats.repair.Amount;
      er.energy.AdditiveModifier -= AdditiveStats.energy.Amount;
      er.recharge.AdditiveModifier -= AdditiveStats.recharge.Amount;
      wm.damage.AdditiveModifier -= AdditiveStats.damage.Amount;
      wm.rof.AdditiveModifier -= AdditiveStats.rof.Amount;

      owningPlayer.moveSpeed.MultiplicativeModifier -= MultiplicitiveStats.speed.Amount;
      a.health.MultiplicativeModifier -= MultiplicitiveStats.health.Amount;
      a.repair.MultiplicativeModifier -= MultiplicitiveStats.repair.Amount;
      er.energy.MultiplicativeModifier -= MultiplicitiveStats.energy.Amount;
      er.recharge.MultiplicativeModifier -= MultiplicitiveStats.recharge.Amount;
      wm.damage.MultiplicativeModifier -= MultiplicitiveStats.damage.Amount;
      wm.rof.MultiplicativeModifier -= MultiplicitiveStats.rof.Amount;
    }

    public override void OnButtonDown()
    {
      
    }

    public override void OnButtonUp()
    {
      
    }
  }
}
