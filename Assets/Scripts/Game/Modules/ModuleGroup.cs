﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Game
{
    public class ModuleGroup : MonoBehaviour
    {
        public Player player;
        public Module initialModulePrefab;

        List<Module> modules = new List<Module>();

        void Reset()
        {
            player = GetComponentInParent<Player>();
        }

        void Awake()
        {
            if (initialModulePrefab)
                Equip(initialModulePrefab, false);
        }

        public void Equip(Module modulePrefab, bool clear)
        {
            if(clear)
            { 
                if (modules.Count > 0)
                {
                    foreach(Module m in modules)
                    { 
                        m.OnRemove();
                        Destroy(modules[0].gameObject);
                    }
                }
                modules.Clear();
            }

            AddModuleRecursive(modulePrefab);
        }

    public bool CheckUpgrade(Module modulePrefab)
    {
      string tree = modulePrefab.TechTree;

      foreach (Module m in modules)
      {
        if (m.TechTree == tree)
          return true;
      }

      return false;
    }

    public void Upgrade(Module modulePrefab)
    {
      string tree = modulePrefab.TechTree;

      foreach(Module m in modules)
      {
        if(m.TechTree == tree && modulePrefab.Tier > m.Tier)
        {
          RemoveModule(m);
          AddModuleRecursive(modulePrefab);
          break;
        }
      }
    }

    protected void AddModuleRecursive(Module m)
    {
      GameObject go = Instantiate(m.gameObject);
      go.transform.SetParent(transform, false);
      go.transform.localPosition = Vector2.zero;
      foreach (var module in go.GetComponentsInChildren<Module>())
      {
        module.owningPlayer = player;
        module.OnEquip();
        modules.Add(module);
      }
    }

    protected void AddModule(Module m)
    {

    }

    protected void RemoveModule(Module m)
    {
      if(modules.Contains(m))
      {
        m.OnRemove();
        Destroy(modules[0].gameObject);
        modules.Remove(m);
      }
    }


        public void OnButtonPressed()
        {
            foreach (var module in modules)
                module.OnButtonDown();
        }

        public void OnButtonReleased()
        {
            foreach (var module in modules)
                module.OnButtonUp();
        }
    }
}