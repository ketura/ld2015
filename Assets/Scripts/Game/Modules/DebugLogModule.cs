﻿using UnityEngine;
using System.Collections;
using System;

namespace Game
{
    public class DebugLogModule : Module
    {
        public override void OnButtonDown()
        {
            Debug.Log(gameObject.name + " button down");
        }

        public override void OnButtonUp()
        {
            Debug.Log(gameObject.name + " button up");
        }

        public override void OnEquip()
        {
            Debug.Log(gameObject.name + " equipping");
        }

        public override void OnRemove()
        {
            Debug.Log(gameObject.name + " removing");
        }
  }
}
