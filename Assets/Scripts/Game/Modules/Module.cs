﻿using UnityEngine;

namespace Game
{
    public enum ModuleType
    {
        Primary,
        Secondary,
        Tertiary,
        Consumable
    }

    public abstract class Module : MonoBehaviour
    {
        [SerializeField]
        private ModuleType type;

        public ModuleType Type { get { return type; } }

        public int Tier;

    public string ModuleName;
    public string Description;
    public Sprite Icon;

        public string TechTree;

        public Player owningPlayer
        {
            get; set;
        }

        public abstract void OnButtonDown();
        public abstract void OnButtonUp();

        public abstract void OnEquip();
        public abstract void OnRemove();
    }
}
