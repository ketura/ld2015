﻿using UnityEngine;
using System.Collections;

namespace Game
{
  [RequireComponent(typeof(Rigidbody2D))]
  [RequireComponent(typeof(BoxCollider2D))]
  [RequireComponent(typeof(SpriteRenderer))]
  public class Projectile : MonoBehaviour, IHomingReceiver
  {
    public float damageAmount;

    public void Countdown(float lifetime)
    {
      StartCoroutine(StartDying(lifetime));
    }

    private bool dying = false;
    IEnumerator StartDying(float lifetime)
    {
      if (!dying)
      {
        dying = true;

        yield return new WaitForSeconds(lifetime);

        //Debug.Log("Dying!" + this);
        Destroy(this.gameObject);

        dying = false;
      }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
      //Debug.Log(other);
      var attackable = other.GetComponent<Attackable>();
      if (attackable)
      {
        attackable.TakeDamage(damageAmount);
        StopCoroutine(StartDying(0));
        //Debug.Log("Hit!");
        Destroy(this.gameObject);
      }
    }


        public void ApplyHoming(Vector3 targetPosition, float rotationSpeed)
        {
            var rigidbody2D = GetComponent<Rigidbody2D>();
            Vector3 dir = targetPosition - transform.position;
            Vector2 fwdDir = transform.up;
            float angDiff = Vector2.Angle(dir, fwdDir);
            Vector3 cross = Vector3.Cross(fwdDir, dir);
            if (cross.z > 0)
            {
                angDiff = 360 - angDiff;
            }
            if (angDiff > 1f)
            {
                rigidbody2D.angularVelocity = angDiff < 180f ? -rotationSpeed : rotationSpeed;
            }
            else
            {
                rigidbody2D.angularVelocity = 0f;
            }

            rigidbody2D.velocity = rigidbody2D.velocity.magnitude * transform.up;
        }
  }

}
