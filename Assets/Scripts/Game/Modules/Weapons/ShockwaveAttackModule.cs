﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

using Vectrosity;

namespace Game
{
    public class ShockwaveAttackModule : Module
    {
        public VectorLine radius;
        public LayerMask layerMask;

        public AudioClip chargeUpSound;
        AudioSource instantiatedChargeUp;
        public AudioClip attackSound;

        public float initialSize;
        public float maxSize;
        public float chargeToMaxTime;
        public float overloadWarningTime;
        public float overloadTime;
        public float overloadWarningShakeStrength;

        public float damageAmount;

        bool charging = false;
        float currentSize;
        float chargeStartTime;

        void Start()
        {
            radius = new VectorLine("shockwave", new List<Vector3>(new Vector3[40]), 2.5f, LineType.Continuous);
            radius.color = Color.red;
      //Debug.Log(CanvasManager.Instance.HUDCanvas.transform);
            radius.SetCanvas(CanvasManager.Instance.HUDCanvas);
      //Debug.Log(radius.)
      //radius.drawTransform.SetParent(CanvasManager.Instance.HUDCanvas.transform, true);
        }

        void Update()
        {
            if (Time.timeScale == 0.0f)
                return;
            Vector3 drawOffset = Vector3.zero;

            if (charging)
            {
                float timeSinceStart = Time.time - chargeStartTime;
                currentSize = Mathf.Lerp(initialSize, maxSize, timeSinceStart / chargeToMaxTime);

                if (timeSinceStart >= overloadTime)
                {
                    //TODO: handle the overload penalty
                    FinishAttack();
                }
                else if (timeSinceStart >= overloadWarningTime)
                {
                    drawOffset = new Vector3(   UnityEngine.Random.Range(-overloadWarningShakeStrength, overloadWarningShakeStrength),
                                                UnityEngine.Random.Range(-overloadWarningShakeStrength, overloadWarningShakeStrength));
                }
            }
            else if (currentSize > 0.0f)
            {
                FinishAttack();
            }

            transform.localScale = new Vector3(currentSize, currentSize, currentSize);

            radius.drawTransform = transform;
            radius.MakeCircle(drawOffset, 1.0f);
            radius.Draw();
        }

        void FinishAttack()
        {
            if(instantiatedChargeUp)
                instantiatedChargeUp.Stop();
            AudioManager.Instance.PlayClip(attackSound);

            foreach (var collider in Physics2D.OverlapCircleAll(transform.position, currentSize, layerMask))
            {
                var attackable = collider.GetComponent<Attackable>();
                if(attackable)
                    attackable.TakeDamage(damageAmount);
            }

            charging = false;
            currentSize = 0.0f;
        }

        public override void OnButtonDown()
        {
            if (!charging)
            {
                charging = true;
                currentSize = initialSize;
                chargeStartTime = Time.time;
                instantiatedChargeUp = AudioManager.Instance.PlayClip(chargeUpSound);
            }
        }

        public override void OnButtonUp()
        {
            charging = false;
        }

        public override void OnEquip()
        {
            //throw new NotImplementedException();
        }

        public override void OnRemove()
        {
            //throw new NotImplementedException();
        }
  }
}