﻿using UnityEngine;
using System.Collections;
using System;

namespace Game
{

  public class ProjectileWeaponModule : Module
  {
    public Projectile ProjectilePrefab;
    public Transform SpawnPoint;
    public AudioClip fireSound;

    public float EnergyCost = 1.0f;

    public float rof = 1.0f;
    public float aoe = 0;
    public float Damage = 50.0f;
    public float Speed = 50.0f;

    public int Volleys = 1;
    public int ShotsPerVolley = 1;
    public float ShotDelay = 0.2f;
    public float VolleyDelay = 0.1f;
    public float ReloadDelay = 0.5f;
    public float ProjectileLifespan = 2.0f;

    public bool SplitShot = false;
    public float SplitOffset = 0.25f;
    public float SpawnOffset = 0.25f;

    private bool firing = false;

    private EnergyReserve energy;

    // Use this for initialization
    void Start()
    {
      GameObject go = FindParentWithTag(gameObject, "Player");
      if(go != null)
        energy = go.GetComponent<EnergyReserve>();
    }

    private GameObject FindParentWithTag(GameObject childObject, string tag)
    {
      Transform t = childObject.transform;
      while (t.parent != null)
      {
        if (t.parent.tag == tag)
          return t.parent.gameObject;
        t = t.parent;
      }
      return null; // Could not find a parent with given tag.
    }

    // Update is called once per frame
    void Update()
    {
            if (Time.timeScale == 0.0f)
                return;
      if (firing)
      {
        //Debug.Log("Firing!");
        StartCoroutine(Fire());
      }
    }

    public override void OnButtonDown()
    {
      if (!firing)
        firing = true;
    }

    public override void OnButtonUp()
    {
      firing = false;
    }

    private bool shot = false;
    IEnumerator Fire()
    {
      if(!shot)
      {
        shot = true;

        if(energy != null)
        {
          if (energy.energy.CurrentEnergy >= EnergyCost)
            //Debug.Log(energy.energy.CurrentEnergy);
            energy.energy.CurrentEnergy -= EnergyCost;
          else
          {
            shot = true;
            yield break;
          }
        }

        float offset = 0;

        if (SplitShot)
        {
          offset = (Volleys * SplitOffset) / -2 + (SplitOffset / 2);
          //Debug.Log(offset);
        }

        for (int i = 0; i < Volleys; ++i)
        {
          for (int j = 0; j < ShotsPerVolley; ++j)
          {
            Vector2 pos = SpawnPoint.localPosition;
            //Debug.Log("pos: " + pos);
            if (SplitShot)
              pos = new Vector2(pos.x + offset + (SplitOffset * j), pos.y);

                        if (fireSound)
                            AudioManager.Instance.PlayClip(fireSound);

            SpawnProjectile(transform.up, pos);
            if (!SplitShot)
              yield return new WaitForSeconds(ShotDelay);
          }
          yield return new WaitForSeconds(VolleyDelay);
        }

        yield return new WaitForSeconds(ReloadDelay);

        shot = false;
      }
    }

    private void SpawnProjectile(Vector2 dir, Vector2 pos)
    {
      Vector2 tpos = owningPlayer.TankDir.RotationVector() * -1;
      Projectile bullet = (Projectile)Instantiate(ProjectilePrefab, pos, Quaternion.Euler(0,0, owningPlayer.TankDir.TurretAngle - 90));
      //Debug.Log(owningPlayer.TankDir.TurretAngle - 90);
      //Debug.Log(bullet.transform.rotation.eulerAngles.z);
      bullet.transform.SetParent(transform);
      bullet.transform.localPosition = pos;
      bullet.transform.SetParent(null, true);

      bullet.damageAmount = Damage;
      //Projectile bullet = go.GetComponent<Projectile>();

      bullet.Countdown(ProjectileLifespan);
      //Debug.Log(tpos);
      bullet.GetComponent<Rigidbody2D>().velocity = bullet.transform.up * Speed;
      //Vector2.up

      //Debug.Log(bullet);

    }

    public override void OnEquip()
    {
      //throw new NotImplementedException();
    }

    public override void OnRemove()
    {
      //throw new NotImplementedException();
    }
  }
}
