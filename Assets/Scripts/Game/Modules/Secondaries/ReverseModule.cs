﻿using UnityEngine;
using System.Collections;
using System;

namespace Game
{
    public class ReverseModule : Module
    {
        public float topSpeed;
        public float acceleration;

        bool decelerating = false;
        float currentSpeed;

        void Update()
        {
            if (decelerating)
                currentSpeed -= acceleration * Time.deltaTime;
            else
                currentSpeed += acceleration * Time.deltaTime;

            currentSpeed = Mathf.Clamp(currentSpeed, -topSpeed, topSpeed);

            owningPlayer.moveSpeed.AdditiveModifier = currentSpeed;
        }

        public override void OnButtonDown()
        {
            decelerating = true;
        }

        public override void OnButtonUp()
        {
            decelerating = false;
        }

        public override void OnEquip()
        {
            throw new NotImplementedException();
        }

        public override void OnRemove()
        {
            throw new NotImplementedException();
        }
  }
}