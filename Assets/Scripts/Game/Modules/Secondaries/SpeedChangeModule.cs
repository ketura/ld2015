﻿using UnityEngine;
using System.Collections;
using System;

namespace Game
{
    public class SpeedChangeModule : Module
    {
        public float baseSpeed;
        public float topSpeed;
        public float acceleration;
        public float deceleration;

        bool accelerating = false;
        float additionalSpeed;

        void Update()
        {
            if (accelerating)
                additionalSpeed += acceleration * Time.deltaTime;
            else
                additionalSpeed = Mathf.Max(0.0f, additionalSpeed - deceleration * Time.deltaTime);

            additionalSpeed = Mathf.Clamp(additionalSpeed, 0.0f, topSpeed - baseSpeed);

            owningPlayer.moveSpeed.AdditiveModifier = baseSpeed + additionalSpeed;
        }

        public override void OnButtonDown()
        {
            accelerating = true;
        }

        public override void OnButtonUp()
        {
            accelerating = false;
        }

        public override void OnEquip()
        {
            throw new NotImplementedException();
        }

        public override void OnRemove()
        {
            throw new NotImplementedException();
        }
  }
}