﻿using UnityEngine;
using System.Collections;

namespace Game
{
    public class HoldModuleButtonForever : MonoBehaviour
    {
        public ModuleGroup moduleGroup;

        private float range = 10f;

        private bool pressed = false;

        // Use this for initialization
        void Start()
        {
        }

        void Update()
        {
            if (TargetForEnemies.Instance == null)
            {
                return;
            }

            var target = TargetForEnemies.Instance;
            float distance = Vector3.Distance(target.transform.position, transform.position);
            bool isInRange = distance < range;

            if (isInRange != pressed)
            {
                pressed = isInRange;

                if (pressed)
                {
                    moduleGroup.OnButtonPressed();
                }
                else
                {
                    moduleGroup.OnButtonReleased();
                }
            }
        }
    }
}