﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

namespace Game
{
    public class InputManager : MonoBehaviour {

        public static InputManager Instance
        {
            get
            {
                return _instance;
            }
        }
        static InputManager _instance;

        [Serializable]
        public class InputButton
        {
            public List<KeyCode> keys = new List<KeyCode>();
            public List<int> mouseButtons = new List<int>();

            public bool Pressed()
            {
                foreach (var key in keys)
                    if (Input.GetKeyDown(key))
                        return true;
                foreach (var button in mouseButtons)
                    if (Input.GetMouseButtonDown(button))
                        return true;

                return false;
            }

            public bool Held()
            {
                foreach (var key in keys)
                    if (Input.GetKey(key))
                        return true;
                foreach (var button in mouseButtons)
                    if (Input.GetMouseButton(button))
                        return true;

                return false;
            }

            public bool Released()
            {
                foreach (var key in keys)
                    if (Input.GetKeyUp(key))
                        return true;
                foreach (var button in mouseButtons)
                    if (Input.GetMouseButtonUp(button))
                        return true;

                return false;
            }
        }

        public InputButton PrimaryButton = new InputButton();
        public InputButton SecondaryButton = new InputButton();

    	// Use this for initialization
    	void Start () {
            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
    	}
    }
}
